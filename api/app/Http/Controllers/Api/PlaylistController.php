<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Playlist; 
class PlaylistController extends Controller
{

    private $URL = 'http://localhost:8000/api/v1/';
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = Playlist::create($request->all()); 
        $resource = $this->URL . 'playlists/' . $data->id; 
        
        return response()->json([$data], 201)->header('Location', $resource);
    }

    /**
     * Display the specified resource filtered by the user id. 
     *
     * @param  int  $id User ID
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $playlist = Playlist::where('user_id', $id)->first();
        $playlist->user_id =  Playlist::find($playlist->id)->user;

        return response()->json($playlist, 200); 
    }
}
