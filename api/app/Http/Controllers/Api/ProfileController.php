<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Profile;
use App\Http\Requests\StoreProfileRequest;

class ProfileController extends Controller
{
    private $URL = 'http://localhost:8000/api/v1/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $userid = $request->query('userid');
        $data = Profile::where('user_id', $userid)->get();

        for ($i = 0; $i < count($data); $i++) {
            $data[$i]->user_id = Profile::find($data[$i]->id)->user;
        }

        return response()->json($data, 200);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreProfileRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProfileRequest $request)
    {
        $data = [];
        $request->validated();
        $date = $request->input('birth');
        if (!$date) {
            $data = Profile::create([
                'name' => $request->input('name'),
                'profile_name' => $request->input('profile_name'),
                'pin' => $request->input('pin'),
                'birth' => $request->input('birth'),
                'user_id' => $request->input('user_id'),
            ]);
        } else {
            $data = Profile::create($request->all());
        }

        if ($data) {
            $data->user_id = Profile::find($data->id)->user;

            $resource = $this->URL . 'profiles/' . $data->id;
            return response()->json([$data], 201)->header('Location', $resource);
        }

        return response()->json(['error' => 'Imposible store the video.'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Profile::find($id);

        if ($data) {
            $data->user_id = Profile::find($data->id)->user;
            return response()->json($data, 200);
        }

        return response()->json(['error' => 'Could not find the profile.'], 500);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $res = Profile::where('id', $id)->update($request->all());

        if ($res > 0) {
            return response()->json(['msg' => 'Updated'], 200);
        }

        return response()->json(['error' => $res], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $res = Profile::where('id', $id)->delete();

        if ($res) {
            return response()->json([], 204);
        }
        return response()->json(['error' => 'Problems deleting the profile.'], 500);
    }
}
