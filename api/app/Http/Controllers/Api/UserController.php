<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Http\Requests\StoreUserRequest;
use App\Models\EmailVerifyToken;
use Authy\AuthyApi;

define('BASE_SG_URL', 'https://api.sendgrid.com/v3/'); 



class UserController extends Controller
{
    private $URL = 'http://localhost:8000/api/v1/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = User::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        $request->validated(); 
        
        //If the passwords are differents
        if($request->input('password') != $request->input('verify_password')){
            return response()->json(['msg' => 'The passwords are different'], 403); 
        }

        $data = User::create($request->all());
        $resource = $this->URL . "users/".$data->id;

        $res = $this->sendVerifyEmail($data->email, $data->name . ' ' . $data->lastname , $data->id);

        $resAuthy = $this->createAuthyUser($data); 

        if($resAuthy){
            //error
            return response()->json(["error" => $resAuthy], 500)->header('Location', $resource);
        }

        if($res['status'] != 202){
            //error
            return response()->json([$data, $res], 500)->header('Location', $resource);
        }
        
        return response()->json([$data], 201)->header('Location', $resource);
        //return response()->json([$res], 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $res = User::findOrFail($id); 
        
        return response()->json($res, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Method to send an email to verify the new account.
     * @param email Email to send. 
     * @param name String name of the new user.
     * @param uid User Id To store the token.  
     * @return JSON Keys: 'status', 'haeders', 'body' (response of the sendgrid api)
     */
    private function sendVerifyEmail($emaill, $name, $uid)
    {
        $token = bcrypt($emaill);
        $url = $this->URL . "users/session/email";
        
        $tokenStore = EmailVerifyToken::create(['token' => $token, 'user_id' => $uid]);
        
        
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom("glo-ware@yopmail.com", "TubeKids Admin");
        $email->setSubject("Confirm your TubeKids account");
        $email->addTo($emaill, $name);
        $email->addContent("text/html", "
        <div style='
            height: 275px;
            width: 400px;
            font-family: Arial;
            border: 2px solid black;
            border-radius: 5px;
            text-align: center;
            font-size: 20px;
            '>
            <p>Please click on the button to verify your account.</p>
            <form action='$url' method='post'>
                <input
                style='
                    background: transparent;
                    border: none;
                    color: transparent;
                '
                type='text' name='token' value='$token' readonly><br>
                <input style='
                background-color: #4CAF50;
                border: none;
                color: white;
                padding: 16px 32px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                margin: 4px 2px;
                cursor: pointer;
                ' type='submit' value='Verify account' />
            </form>
            <br><br>
            <strong>TubeKids - Costa Rica </strong>
        </div>
        ");
        
        $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));
        
        try {
            $response = $sendgrid->send($email);

            return ['status' => $response->statusCode() , 'headers' => $response->headers(), 'body' => $response->body()]; 
            
        } catch (Exception $e) {
            echo 'Caught exception: '. $e->getMessage() ."\n";
        }
        
    }

    /**
     * Method to create the authy/twilio 2OAuth.
     * @param userCreated user stored in the database. 
     * @return Array of errors or null if was successful 
     */
    private function createAuthyUser($userCreated){
        
        $authy_api = new AuthyApi(env('TWILIO_AUTHY_API_KEY'));
        $user = $authy_api->registerUser($userCreated->email, $userCreated->phone, $userCreated->country_code);
        

        if($user->ok()){
            //store the user
            User::where('id', $userCreated->id)
            ->update(['authy_id' => $user->id()]);
        }else{
            return $user->errors();
        }

        return null; 
    }
}
