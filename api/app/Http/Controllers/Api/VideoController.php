<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Video;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\StoreVideoRequest;


class VideoController extends Controller
{
    private $URL = 'http://localhost:8000/api/v1/';
    /**
     * Display a listing of the resource.
     * @param Request Expects query strings (*)playlist_id and name (not required) to search by name
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $playlist = $request->query('playlist_id');
        $name = $request->query('name');
        $data = []; 
        if($name){
            $data = Video::where('playlist_id', $playlist)->where('name', 'like', "%$name%")->get();
        }else{
            $data = Video::where('playlist_id', $playlist)->get();
        }

        for ($i = 0; $i < count($data); $i++) {
            $data[$i]->playlist_id = Video::find($data[$i]->id)->playlist;
        }

        return response()->json($data, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreVideoRequest $request)
    {   
        
        $request->validated(); 
        //Verify if the has a file or no.
        $isYouTube = $request->input('is_youtube');
        if ($isYouTube < 1) {
            if(!$request->input('url') && trim($request->input('url'))  != '' ){
                return response()->json(['error' => 'Imposible store the YouTube video without a url.'], 500);
            }
            //Is not a youtube video
            if ($request->hasFile('file')) {
                $fileName = $request->input('file_name'); 
                $path = Storage::putFileAs('public/videos', $request->file('file'), $fileName);
                $request->merge(['url' => $path]);
            }
            //*****
        }

        $data = Video::create($request->all());
        $resource = $this->URL . 'videos/' . $data->id;

        if ($data) {
            return response()->json([$data], 201)->header('Location', $resource);
        }

        return response()->json(['error' => 'Imposible store the video.'], 500);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Video::find($id);

        $data->playlist_id = Video::find($id)->playlist;


        return response()->json($data, 200);
    }

    /**
     * Get the videos that could match with a name request
     * @param Request waiting playlist id query string and name query string
     * @return \Illuminate\Http\Response
     */
    public function showByName(Request $request)
    {
        $playlist = $request->query('playlist_id');
        $name = $request->query('name');
        $data = Video::where('playlist_id', $playlist)->where('name', 'like', "%$name%")->get();

        for ($i = 0; $i < count($data); $i++) {
            $data[$i]->playlist_id = Video::find($data[$i]->id)->playlist;
        }

        return response()->json($data, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $oldVideo = Video::find($id);

        $isYouTube = $request->input('is_youtube');

        if ($request->hasFile('file') || $request->input('url')) {
            //It's going to update the video
            //Delete the old video in case if is a video stored in the server.
            if ($oldVideo->is_youtube < 1) {
                //The video must be deleted from the storage
                Storage::delete($oldVideo->url);
            }
        }

        if ($isYouTube < 1) {
            //Is not a youtube video
            if ($request->hasFile('file')) {

                $path = Storage::putFileAs('public/images/', $request->file('video'));

                $request->merge(['url' => $path]);
            }
            //*****
        }

        $res = Video::where('id', $id)
            ->update($request->except(['file']));

        if ($res > 0) {
            return response()->json($request->all(), 200);
        }

        return response()->json(['Error' => 'Problems updating the video.', 500]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $res = Video::where('id', $id);
        $data = $res->first();

        if ($data->is_youtube < 1) {
            //The video must be deleted from the storage
            Storage::delete($data->url);
        }

        $res->delete();

        return response()->json([], 204);
    }
}
