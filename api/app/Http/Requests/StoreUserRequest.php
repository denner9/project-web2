<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {   
        return true; 
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $time = strtotime((date('Y')-18).date('-m-d'));
        $dateUnder18 = date('Y-m-d', $time);

        return [
            'name' => 'required',
            'lastname' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'birth' => "required|before_or_equal:$dateUnder18",
            'phone' => 'required'
        ];
    }
}
