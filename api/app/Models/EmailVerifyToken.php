<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailVerifyToken extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'token', 'user_id'
    ];

    

}
