<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Playlist extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'user_id', 'active'
    ];

    protected $attributes = ['active' => true];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}
