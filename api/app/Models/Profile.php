<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'profile_name', 'active', 'pin', 'birth', 'user_id'
    ];

    protected $attributes = ['active' => true];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
