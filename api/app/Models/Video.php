<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'url', 'active', 'is_youtube', 'playlist_id'
    ];

    protected $attributes = ['active' => true];

    public function playlist(){
        return $this->belongsTo('App\Models\Playlist');
    }
}
