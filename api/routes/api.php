<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', ['namespace' => 'App\Http\Controllers\Api'], function ($api) {

    $api->group(['prefix' => 'v1'], function ($api) {
        $api->group(['middleware' => 'cors'], function ($api) {
            $api->post('users/session', 'Auth\AuthController@login');
            $api->post('users/session/email', 'Auth\AuthController@validateEmail');
            $api->post('users/session/code', 'Auth\AuthController@verify2OAuthToken');
            $api->post('users', 'UserController@store');
            $api->post('playlists', 'PlaylistController@store');
            //$api->post('users/session/new', 'Auth\AuthController@refesh');
            $api->group(['middleware' => ['jwt.auth']], function ($api) {
                $api->delete('users/session', 'Auth\AuthController@logout');
                $api->get('users/{id}', 'UserController@show');
                $api->get('playlists/{id}', 'PlaylistController@show'); 
                $api->resource('videos', 'VideoController');
                $api->resource('profiles', 'ProfileController');
            });
        });
    });

});
